package com.example.myapplication.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TopRatedMovies {



    @SerializedName("total_pages")
    @Expose
    private Integer totalPages;
    @SerializedName("results")
    @Expose
    private  List<Result> results;







    public Integer getTotalPages() {
        return totalPages;
    }



    public List<Result> getResults() {
        return results;
    }



}
