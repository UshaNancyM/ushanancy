package com.example.myapplication.utilitis;

import android.app.Application;

import com.example.greendaogenerator.DaoMaster;
import com.example.greendaogenerator.DaoSession;
import com.facebook.stetho.Stetho;

public class GreenDao extends Application {
    private DaoSession daoSession;

    @Override
    public void onCreate() {
        super.onCreate();
        Stetho.initializeWithDefaults(this);
        daoSession=new DaoMaster(new DaoMaster.OpenHelper(this,"greendaogenerator.db").getWritableDb()).newSession();
    }

    public DaoSession getDaoSession() {
        return daoSession;
    }
}
