package com.example.myapplication.activities;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestOptions;
import com.example.myapplication.adapter.MovieListAdapter;
import com.example.myapplication.R;

import java.util.Objects;

public class MovieDetailActivity extends AppCompatActivity {
    private String title;
    private String poster;
   private String overview;
   private String releaseDate;
   private int vote;

    private double popularity;
    private ImageView posterImage;
    private ImageView backDropImage;
    private TextView titleText;
    private TextView overviewText;
    private TextView releaseDateText;
    private String backDrop;
    private TextView voteText;
    private TextView popularityText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_detail);
        title = getIntent().getStringExtra("title");
        poster = getIntent().getStringExtra("poster");
        overview = getIntent().getStringExtra("overview");
        releaseDate = getIntent().getStringExtra("releaseDate");
        backDrop = getIntent().getStringExtra("backDrop");
        vote = getIntent().getIntExtra("vote",0);
        popularity = getIntent().getDoubleExtra("popularity",0.0);

        init();
        Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00a5ff")));
        setView();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().getSharedElementEnterTransition().setDuration(500);
            getWindow().getSharedElementReturnTransition().setDuration(500);
        }

    }

    private void setView() {
        int radius = (int) getResources().getDimension(R.dimen.radiusImage);
        titleText.setText(title);
        voteText.setText(String.valueOf(vote));
        popularityText.setText(String.valueOf( popularity));
        releaseDateText.setText(releaseDate);
        overviewText.setText(overview);
        Glide.with(getApplicationContext())
                .load(MovieListAdapter.BASE_URL_IMG + poster).apply(new RequestOptions().transform(new RoundedCorners(radius)))
                .transition(DrawableTransitionOptions.withCrossFade()).into(posterImage);
        Glide.with(getApplicationContext())
                .load(MovieListAdapter.BASE_URL_IMG + backDrop)
                .transition(DrawableTransitionOptions.withCrossFade()).into(backDropImage);
    }

    private void init() {
        overviewText = findViewById(R.id.overview);
        releaseDateText = findViewById(R.id.release_date);
        voteText = findViewById(R.id.vote);
        titleText = findViewById(R.id.title);
        popularityText = findViewById(R.id.popularity);
        posterImage = findViewById(R.id.image);
        backDropImage = findViewById(R.id.poster_image);


    }
}
