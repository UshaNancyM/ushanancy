package com.example.myapplication.fragments;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.greendaogenerator.User;
import com.example.greendaogenerator.UserDao;
import com.example.myapplication.R;
import com.example.myapplication.activities.MainActivity;
import com.example.myapplication.utilitis.GreenDao;
import com.example.myapplication.utilitis.SharedPref;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.Objects;


public class LoginFragment extends Fragment implements View.OnClickListener, View.OnFocusChangeListener {
    private View view;
    private TextInputEditText name;
    private TextInputEditText password;
    private TextInputLayout passwordLayout;
    private Button login;
    private TextView forgetPassword;
    private TextView signUp;
    private Fragment fragment;
    private String storedPassword;
    private String storedName;
    private boolean validateName = false;
    private boolean validatePass = false;
    private UserDao userDao;
    private ProgressDialog progressDialog;
    private String strName;
    private String strPassword;
    private Long key;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_login, container, false);

        init();

        passwordLayout.setErrorEnabled(false);
        login.setOnClickListener(this);
        signUp.setOnClickListener(this);
        forgetPassword.setOnClickListener(this);
        String forgetText = (String) forgetPassword.getText();
        SpannableString spannableStr = new SpannableString(forgetText);
        UnderlineSpan underlineSpan = new UnderlineSpan();
        spannableStr.setSpan(underlineSpan, 0, 15, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        forgetPassword.setText(spannableStr);


        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Loading Please wait");
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);


        return view;
    }

    private void init() {
        userDao = ((GreenDao) Objects.requireNonNull(getActivity()).getApplication()).getDaoSession().getUserDao();
        name = view.findViewById(R.id.editTextName);
        password = view.findViewById(R.id.editTextPassword);
        login = view.findViewById(R.id.login_button);
        forgetPassword = view.findViewById(R.id.forgetPassword);
        signUp = view.findViewById(R.id.signUp_from_login);
        passwordLayout = view.findViewById(R.id.textInputPassword);
        name.setOnFocusChangeListener(this);
        password.setOnFocusChangeListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.login_button:

                progressDialog.show();
                validation();
                break;
            case R.id.signUp_from_login:

                fragment = new SignUpFragment();
                callFragment();
                break;
            case R.id.forgetPassword:

                fragment = new ForgetPasswordFragment();
                callFragment();
                break;

            default:
        }
    }

    private void validation() {
        progressDialog.dismiss();
        strPassword = Objects.requireNonNull(password.getText()).toString().trim();
        strName = Objects.requireNonNull(name.getText()).toString().trim();
        checkCondition();
        if (validateName && validatePass) {
            Intent intent = new Intent(getContext(), MainActivity.class);
            startActivity(intent);
            Objects.requireNonNull(getActivity()).finish();
            SharedPref.saveLoginStatus(getActivity(), true);
            SharedPref.saveKey(getActivity(), key);
        } else {
            name.setError("invalid");
            passwordLayout.setError("invalid");
        }
    }

    private void passwordValidate() {


        if (strPassword.isEmpty()) {
                    passwordLayout.setError("password cannot be empty");
                    validatePass = false;

        } else if (!strPassword.equals(storedPassword)) {
                    passwordLayout.setError("invalid password");
                    validatePass = false;

                } else {
                    passwordLayout.setError(null);
                    passwordLayout.setErrorEnabled(false);
                    validatePass = true;
                }


    }

    private void callFragment() {
        FragmentManager manager = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.container, fragment).addToBackStack(null).commit();
    }

    private void nameValidate() {


        if (strName.isEmpty()) {
                    name.setError("name cannot be empty");
                    validateName = false;

        } else if (!strName.equals(storedName)) {
                    name.setError("invalid name");
                    validateName = false;

                } else {
                    name.setError(null);

                    validateName = true;


        }
    }

    private void checkCondition() {
        User user = userDao
                .queryBuilder().where(UserDao.Properties.User_name.eq(strName)).build().unique();

        if (user != null) {
            storedPassword = user.getPassword();
            storedName = user.getUserName();

            key = user.getId();
            passwordValidate();
            nameValidate();
        }

    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (v.getId() == R.id.editTextName) {
            if (hasFocus) {
                name.setError(null);
            }
        } else {
            if (hasFocus) {
                passwordLayout.setError(null);
                passwordLayout.setErrorEnabled(false);
            }
        }
    }
}
