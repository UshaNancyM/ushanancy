package com.example.myapplication.activities;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.greendaogenerator.User;
import com.example.greendaogenerator.UserDao;
import com.example.myapplication.R;
import com.example.myapplication.utilitis.GreenDao;
import com.example.myapplication.utilitis.SharedPref;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.Objects;

public class ChangePasswordActivity extends AppCompatActivity implements View.OnFocusChangeListener {
    private TextInputLayout newPasswordLayout;
    private TextInputLayout confirmPasswordLayout;
    private TextInputLayout oldPasswordLayout;
    private TextInputEditText newPassword;
    private TextInputEditText confirmPassword;
    private TextInputEditText oldPassword;
    private Button save;
    private UserDao userDao;
    private User user;
    private String strOldPassword;
    private String strNewPassword;
    private String strConfirmPassword;
    private String passwordPattern;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00a5ff")));
        long key = SharedPref.shareKey(getApplicationContext());
        userDao = ((GreenDao) getApplication()).getDaoSession().getUserDao();
        user = userDao
                .queryBuilder().where(UserDao.Properties.Id.eq(key)).build().unique();
        init();

        save.setOnClickListener(v -> validate());
    }

    private void validate() {

        strOldPassword = Objects.requireNonNull(oldPassword.getText()).toString().trim();
        strNewPassword = Objects.requireNonNull(newPassword.getText()).toString().trim();
        strConfirmPassword = Objects.requireNonNull(confirmPassword.getText()).toString().trim();
        validateOld();
        validateNew();
        validateConfirm();
        if (validateConfirm() && validateNew() && validateOld()) {
            user.setPassword(strNewPassword);
            userDao.update(user);
            Intent intent = new Intent(ChangePasswordActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
        } else {

            Toast.makeText(this, "Provide correct details", Toast.LENGTH_SHORT).show();
        }

    }

    private boolean validateConfirm() {
        if (strConfirmPassword.isEmpty()) {
            confirmPasswordLayout.setError("confirm password cannot be empty");
            return false;

        } else if (!strConfirmPassword.equals(strNewPassword)) {
            confirmPasswordLayout.setError("confirm password and password should be same");
            return false;

        } else {
            confirmPasswordLayout.setError(null);
            confirmPasswordLayout.setErrorEnabled(false);

            return true;
        }
    }

    private boolean validateNew() {
        if (strNewPassword.isEmpty()) {
            newPasswordLayout.setError("password cannot be empty");
            return false;

        } else if (!strNewPassword.matches(passwordPattern)) {
            newPasswordLayout.setError("Password should contain one capital letter,one small letter, one number,one special character and size between 6to12");
            return false;

        } else if (strNewPassword.equals(strOldPassword)) {
            newPasswordLayout.setError("new password should not be as same as old password");
            return false;
        } else {
            newPasswordLayout.setError(null);
            newPasswordLayout.setErrorEnabled(false);

            return true;
        }
    }

    private boolean validateOld() {
        if (!strOldPassword.equals(user.getPassword()) || strOldPassword.isEmpty()) {
            oldPasswordLayout.setError("invalid old password");

            return false;
        } else {
            oldPasswordLayout.setError(null);
            oldPasswordLayout.setErrorEnabled(false);
            return true;
        }

    }

    private void init() {
        passwordPattern = getString(R.string.password_pattern);
        newPassword = findViewById(R.id.editTextPassword);
        oldPassword = findViewById(R.id.editTextOldPassword);
        confirmPassword = findViewById(R.id.editTextPasswordConfirm);
        save = findViewById(R.id.save_button);
        newPasswordLayout = findViewById(R.id.textInputPassword);
        confirmPasswordLayout = findViewById(R.id.textInputPasswordConfirm);
        oldPasswordLayout = findViewById(R.id.textInputOldPassword);

        oldPassword.setOnFocusChangeListener(this);
        newPassword.setOnFocusChangeListener(this);
        confirmPassword.setOnFocusChangeListener(this);

    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        switch (v.getId()) {
            case R.id.editTextPassword:
                if (hasFocus) {
                    newPasswordLayout.setError(null);
                    newPasswordLayout.setErrorEnabled(false);
                }
                break;
            case R.id.editTextOldPassword:
                if (hasFocus) {
                    oldPasswordLayout.setError(null);
                    oldPasswordLayout.setErrorEnabled(false);
                }
                break;
            case R.id.editTextPasswordConfirm:
                if (hasFocus) {
                    confirmPasswordLayout.setError(null);
                    confirmPasswordLayout.setErrorEnabled(false);
                }
                break;
            default:
        }

    }
}
