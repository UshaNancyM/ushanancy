package com.example.myapplication.fragments;


import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.greendaogenerator.User;
import com.example.greendaogenerator.UserDao;
import com.example.myapplication.R;
import com.example.myapplication.activities.MainActivity;
import com.example.myapplication.utilitis.GreenDao;
import com.example.myapplication.utilitis.SharedPref;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.io.IOException;
import java.util.Calendar;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;


public class SignUpFragment extends Fragment implements View.OnClickListener {

    private View view;
    private TextInputEditText name;
    private TextInputEditText email;

    private TextInputLayout passwordLayout;

    private TextInputLayout confirmPasswordLayout;
    private TextInputEditText age;
    private Switch emailUpdate;
    private RadioButton male;
    private RadioButton female;
    private TextInputEditText confirmPassword;
    private TextInputEditText password;
    private Button signUp;
    private TextView login;
    private String strName;
    private String strPassword;
    private String strEmail;
    private String strAge;
    private String imagePath;
    private String passwordPattern;

    private String strGender = "";
    private String strEmailUpdate = "";
    private Fragment fragment;
    private CircleImageView profileImage;
    private ProgressDialog progressDialog;
    private boolean validateName = false;
    private boolean validatePass = false;
    private boolean validateEmail = false;
    private boolean validateConfirmPass = false;

    private static final int PERMISSION_REQUEST_CODE = 100;
    public SignUpFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_sign_up, container, false);
        init(); // initialise all the components
        signUp.setOnClickListener(this);
        age.setOnClickListener(this);
        login.setOnClickListener(this);
        profileImage.setOnClickListener(this);


        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Loading Please wait");
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);


        return view;
    }

    private void init() {
        passwordPattern = getString(R.string.password_pattern);
        name = view.findViewById(R.id.editTextName);
        name.requestFocus();
        InputMethodManager imm = (InputMethodManager) Objects.requireNonNull(getActivity()).getSystemService(Context.INPUT_METHOD_SERVICE);
        Objects.requireNonNull(imm).showSoftInput(name, InputMethodManager.SHOW_IMPLICIT);
        email = view.findViewById(R.id.editTextEmail);
        confirmPassword = view.findViewById(R.id.editText_ConfirmPassword);
        password = view.findViewById(R.id.editTextPassword);
        passwordLayout = view.findViewById(R.id.textInputPassword);
        confirmPasswordLayout = view.findViewById(R.id.textInput_ConfirmPassword);
        age = view.findViewById(R.id.editTextAge);
        male = view.findViewById(R.id.male);
        female = view.findViewById(R.id.female);
        signUp = view.findViewById(R.id.signUp_button);
        login = view.findViewById(R.id.login_from_sign);
        emailUpdate = view.findViewById(R.id.email_update);
        profileImage = view.findViewById(R.id.profile_image);
        male.setChecked(true);

        nameValidate();
        passwordValidate();
        confirmPasswordValidate();
        emailValidate();

    }



    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onClick(View v) {


        switch (v.getId()) {
            case R.id.signUp_button:

                progressDialog.show();
                validation();
                break;
            case R.id.login_from_sign:

                fragment = new LoginFragment();
                callFragment();
                break;
            case R.id.profile_image:
                if (ContextCompat.checkSelfPermission(Objects.requireNonNull(getActivity()), READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(getActivity(), WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {

                    ActivityCompat.requestPermissions(getActivity(), new String[]{READ_EXTERNAL_STORAGE, WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);

                } else {
                    gallery();
                }
                break;
            case R.id.editTextAge:
                getAge();
                break;
            default:
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void getAge() {

        // Get Current Date
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(Objects.requireNonNull(getContext()),
                (group, year, monthOfYear, dayOfMonth) -> {


                    if (year < mYear) {
                        int age1 = mYear - year;
                        age.setText(new StringBuilder().append(age1).append(" years"));
                    }

                }, mYear, mMonth, mDay);

        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.show();
    }

    private void gallery() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(galleryIntent, 101);
    }

    private void validation() {
        progressDialog.dismiss();
        strAge = Objects.requireNonNull(age.getText()).toString().trim();
        strEmail = Objects.requireNonNull(email.getText()).toString().trim();
        strName = Objects.requireNonNull(name.getText()).toString().trim();
        strPassword = Objects.requireNonNull(password.getText()).toString().trim();

        if (male.isChecked()) {
            strGender = "Male";


        } else if (female.isChecked()) {
            strGender = "Female";


        }
        emailUpdate.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (emailUpdate.isChecked()) {
                strEmailUpdate = "Yes";

            } else {
                strEmailUpdate = "No";

            }
        });
        if (!validateName) {
            name.setError("");
        } else if (!validateEmail) {
            email.setError("");
        } else if (!validatePass) {
            passwordLayout.setError("");
        } else if (!validateConfirmPass) {
            confirmPasswordLayout.setError("");
        } else {
            storeData();
        }

    }

    private void storeData() {
        UserDao userDao = ((GreenDao) Objects.requireNonNull(getActivity()).getApplication()).getDaoSession().getUserDao();

        User user = new User();
        user.setUserName(strName);
        user.setEmail(strEmail);
        user.setPassword(strPassword);
        user.setGender(strGender);
        user.setAge(strAge);
        user.setEmailUpdates(strEmailUpdate);
        user.setProfileImage(imagePath);
        userDao.insert(user);

        progressDialog.dismiss();

        Intent intent = new Intent(getContext(), MainActivity.class);
        startActivity(intent);
        getActivity().finish();
        SharedPref.saveKey(Objects.requireNonNull(getContext()), user.getId());
    }

    private void callFragment() {
        FragmentManager manager = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.container, fragment).commit();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 101 && data != null) {
            Uri contentURI = data.getData();
            try {
                imagePath = getRealPathFromURI(contentURI);
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(Objects.requireNonNull(getActivity()).getContentResolver(), contentURI);
                profileImage.setImageBitmap(bitmap);

            } catch (IOException e) {
                Log.e("exception", Objects.requireNonNull(e.getMessage()));
                Toast.makeText(getContext(), "Failed!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private String getRealPathFromURI(Uri uri) {
        Cursor cursor = null;
        try {
            cursor = Objects.requireNonNull(getActivity()).getContentResolver().query(uri, null, null, null, null);
            assert cursor != null;
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(idx);

        } finally {
            Objects.requireNonNull(cursor).close();
        }

    }

    private void confirmPasswordValidate() {
        confirmPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
/*
for before text change  but here we use only after text change
 */
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
/*
for on text change  but here we use only after text change
 */
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().isEmpty()) {
                    confirmPasswordLayout.setError("confirm password cannot be empty");
                    validateConfirmPass = false;

                } else if (!s.toString().equals(strPassword)) {
                    confirmPasswordLayout.setError("confirm password and password should be same");
                    validateConfirmPass = false;

                } else {
                    confirmPasswordLayout.setError(null);
                    confirmPasswordLayout.setErrorEnabled(false);

                    validateConfirmPass = true;
                }
            }
        });
    }

    private void emailValidate() {

        email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
/*
for before text change  but here we use only after text change
 */
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
/*
for on text change  but here we use only after text change
 */
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().isEmpty()) {
                    email.setError("password cannot be empty");
                    validateEmail = false;

                } else if (!Patterns.EMAIL_ADDRESS.matcher(s.toString()).matches()) {
                    email.setError("invalid email");
                    validateEmail = false;

                } else {
                    email.setError(null);


                    validateEmail = true;
                }
            }
        });
    }


    private void passwordValidate() {


        password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
/*
for before text change  but here we use only after text change
 */
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
/*
for on text change  but here we use only after text change
 */
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().isEmpty()) {
                    passwordLayout.setError("password cannot be empty");
                    validatePass = false;

                } else if (!s.toString().matches(passwordPattern)) {
                    passwordLayout.setError("Password should contain one capital letter,one small letter, one number,one special character and size between 6to12");
                    validatePass = false;

                } else {
                    passwordLayout.setError(null);
                    passwordLayout.setErrorEnabled(false);

                    strPassword = s.toString().trim();
                    validatePass = true;
                }
            }
        });
    }

    private void nameValidate() {
        name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
/*
for before text change  but here we use only after text change
 */
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
/*
for on text change  but here we use only after text change
 */

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().isEmpty()) {
                    name.setError("name cannot be empty");
                    validateName = false;

                } else if ((s.toString().length() < 3)) {
                    name.setError("invalid name");
                    validateName = false;

                } else {
                    name.setError(null);

                    validateName = true;
                }

            }

        });

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                gallery();
                Toast.makeText(getActivity(), "Permission granted", Toast.LENGTH_SHORT).show();
            } else {

                Toast.makeText(getActivity(), "Permission denied", Toast.LENGTH_SHORT).show();

            }

        }
    }
}

