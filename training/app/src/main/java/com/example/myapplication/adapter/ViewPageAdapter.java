package com.example.myapplication.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.myapplication.fragments.MovieFragment1;
import com.example.myapplication.fragments.MovieFragment2;
import com.example.myapplication.fragments.MovieFragment3;

public class ViewPageAdapter extends FragmentPagerAdapter {

    // tab titles
    private final String[] tabTitles = new String[]{"Tab1", "Tab2", "Tab3"};
    public ViewPageAdapter(@NonNull FragmentManager fm, int behavior) {
        super(fm, behavior);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        Fragment fragment = new Fragment();
        if (position == 0) {
            fragment = new MovieFragment1();
        } else if (position == 1) {
            fragment = new MovieFragment2();
        } else if (position == 2) {
            fragment = new MovieFragment3();
        }

        return fragment;
    }

    @Override
    public int getCount() {
        return 3;
    }
    // overriding getPageTitle()
    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }


}
