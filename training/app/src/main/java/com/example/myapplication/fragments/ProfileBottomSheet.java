package com.example.myapplication.fragments;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;

import com.example.greendaogenerator.User;
import com.example.greendaogenerator.UserDao;
import com.example.myapplication.R;
import com.example.myapplication.activities.MyProfileActivity;
import com.example.myapplication.utilitis.GreenDao;
import com.example.myapplication.utilitis.SharedPref;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.Objects;

public class ProfileBottomSheet extends BottomSheetDialogFragment implements View.OnClickListener {
    private TextInputEditText name;
    private TextInputEditText email;
    private TextInputLayout nameLayout;
    private TextInputLayout emailLayout;
    private boolean validateName=true ;
    private boolean validateEmail =true;
    private View view;
    private UserDao userDao;
    private User user;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

         view = inflater.inflate(R.layout.my_dialog, container,false);
        long key = SharedPref.shareKey(Objects.requireNonNull(getContext()));
        userDao = ((GreenDao) Objects.requireNonNull(getActivity()).getApplication()).getDaoSession().getUserDao();
        user = userDao
                .queryBuilder().where(UserDao.Properties.Id.eq(key)).build().unique();
         init();
        if (MyProfileActivity.getChange().equals("name")) {
             nameLayout.setVisibility(View.VISIBLE);
             emailLayout.setVisibility(View.GONE);
         } else if (MyProfileActivity.getChange().equals("email")) {
             nameLayout.setVisibility(View.GONE);
             emailLayout.setVisibility(View.VISIBLE);
         }

        return view;
    }

    private void init() {
        MaterialButton save = view.findViewById(R.id.save);
        MaterialButton cancel = view.findViewById(R.id.cancel);
        name=view.findViewById(R.id.name_editText);
        nameLayout=view.findViewById(R.id.name_layout);

        email=view.findViewById(R.id.email_editText);
        emailLayout=view.findViewById(R.id.email_layout);


        if (user != null) {
            name.setText(user.getUserName());
            email.setText(user.getEmail());
        }
        name.setSelectAllOnFocus(true);
        email.setSelectAllOnFocus(true);
        save.setOnClickListener(this);
        cancel.setOnClickListener(this);

        name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
/*
for before text change  but here we use only after text change
 */
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
/*
for on text change  but here we use only after text change
 */

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (s.toString().isEmpty()) {
                    name.setError("name cannot be empty");
                    validateName = false;

                } else if ((s.toString().length() < 3)) {
                    name.setError("invalid name");
                    validateName = false;

                } else {
                    name.setError(null);

                    validateName = true;
                }
            }

        });
        email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
/*
for before text change  but here we use only after text change

 */


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
/*
for on text change  but here we use only after text change
 */

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (s.toString().isEmpty()) {
                    email.setError("email cannot be empty");
                    validateEmail = false;

                } else if (!Patterns.EMAIL_ADDRESS.matcher(s.toString()).matches()) {
                    email.setError("invalid email");
                    validateEmail = false;

                } else {
                    email.setError(null);


                    validateEmail = true;
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.save) {
            validate();
        } else if (v.getId() == R.id.cancel) {
            Objects.requireNonNull(getDialog()).dismiss();
        }

    }

    private void validate() {

        if (MyProfileActivity.getChange().equals("name")&&validateName) {
            user.setUserName(Objects.requireNonNull(name.getText()).toString().trim());
            userDao.update(user);
            Objects.requireNonNull(getDialog()).dismiss();
        } else if (validateEmail&&MyProfileActivity.getChange().equals("email")) {
            user.setEmail(Objects.requireNonNull(email.getText()).toString().trim());
            userDao.update(user);
            Objects.requireNonNull(getDialog()).dismiss();
        }
    }
}
