package com.example;


import org.greenrobot.greendao.generator.DaoGenerator;
import org.greenrobot.greendao.generator.Entity;
import org.greenrobot.greendao.generator.Schema;

public class MyGenerator {
    public static void main(String[] args) throws Exception {
        Schema schema = new Schema(1, " com.example.greendaogenerator");
        adduser(schema);
        schema.enableKeepSectionsByDefault();
        new DaoGenerator().generateAll(schema,"app/src/main/java" );
    }

    // This is use to describe the colums of your table
    private static Entity adduser(final Schema schema) {
        Entity user = schema.addEntity("User");
        user.addIdProperty().primaryKey().autoincrement();
        user.addStringProperty("user_name");
        user.addStringProperty("password");
        user.addStringProperty("gender");
        user.addStringProperty("profile_image");
        user.addStringProperty("email");
        user.addStringProperty("age");
        user.addStringProperty("email_updates");
        return user;
    }
}
