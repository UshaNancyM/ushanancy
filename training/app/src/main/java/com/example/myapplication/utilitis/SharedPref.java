package com.example.myapplication.utilitis;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

public class SharedPref extends Application {

   private static SharedPreferences sharedpreferences;
    private static final String PREF = "store";

    private static final String NAME = "name";
    private static final String EMAIL = "email";
    private static final String PASSWORD = "password";
    private static final String IMAGE_PATH = "imagePath";
    private static final String EMAIL_UPDATES = "emailUpdates";
    private static final String GENDER = "gender";
    private static final String AGE = "age";
    private static final String LOGGED_IN = "loggedIn";
    private static final String PRIMARY_KEY = "primaryKey";



    public static void saveName(Context context, String city) {
        sharedpreferences = context.getSharedPreferences(PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(NAME, city);
        editor.apply();
    }


    public static String shareName(Context ctx) {
        sharedpreferences = ctx.getSharedPreferences(PREF, Context.MODE_PRIVATE);
        return sharedpreferences.getString(NAME, "admin");
    }

    public static void saveEmail(Context context, String city) {
        sharedpreferences = context.getSharedPreferences(PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(EMAIL, city);
        editor.apply();
    }


    public static String shareEmail(Context ctx) {
        sharedpreferences = ctx.getSharedPreferences(PREF, Context.MODE_PRIVATE);
        return sharedpreferences.getString(EMAIL, "jeyanancy11@gmail.com");
    }

    public static void savePassword(Context context, String city) {
        sharedpreferences = context.getSharedPreferences(PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(PASSWORD, city);
        editor.apply();
    }


    public static String sharePassword(Context ctx) {
        sharedpreferences = ctx.getSharedPreferences(PREF, Context.MODE_PRIVATE);
        return sharedpreferences.getString(PASSWORD, "Admin@123");
    }
    public static void saveKey(Context context, Long city) {
        sharedpreferences = context.getSharedPreferences(PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putLong(PRIMARY_KEY, city);
        editor.apply();
    }


    public static long shareKey(Context ctx) {
        sharedpreferences = ctx.getSharedPreferences(PREF, Context.MODE_PRIVATE);
        return sharedpreferences.getLong(PRIMARY_KEY,0L);
    }

    public static void saveGender(Context context, String city) {
        sharedpreferences = context.getSharedPreferences(PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(GENDER, city);
        editor.apply();
    }



    public static void saveAge(Context context, String city) {
        sharedpreferences = context.getSharedPreferences(PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(AGE, city);
        editor.apply();
    }


    public static String shareAge(Context ctx) {
        sharedpreferences = ctx.getSharedPreferences(PREF, Context.MODE_PRIVATE);
        return sharedpreferences.getString(AGE, "20 years");
    }

    public static void saveEmailUpdate(Context context, String city) {
        sharedpreferences = context.getSharedPreferences(PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(EMAIL_UPDATES, city);
        editor.apply();
    }



    public static void saveImage(Context context, String city) {
        sharedpreferences = context.getSharedPreferences(PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(IMAGE_PATH, city);
        editor.apply();
    }


    public static String shareImage(Context ctx) {
        sharedpreferences = ctx.getSharedPreferences(PREF, Context.MODE_PRIVATE);
        return sharedpreferences.getString(IMAGE_PATH, "");
    }

    public static void saveLoginStatus(Context context, boolean status) {
        sharedpreferences = context.getSharedPreferences(PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putBoolean(LOGGED_IN, status);
        editor.apply();
    }


    public static boolean shareLoginStatus(Context ctx) {
        sharedpreferences = ctx.getSharedPreferences(PREF, Context.MODE_PRIVATE);
        return sharedpreferences.getBoolean(LOGGED_IN, false);
    }
}
