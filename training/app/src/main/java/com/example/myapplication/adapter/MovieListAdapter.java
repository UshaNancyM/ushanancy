package com.example.myapplication.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.app.ActivityOptionsCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.myapplication.R;
import com.example.myapplication.activities.MovieDetailActivity;
import com.example.myapplication.model.Result;

import java.util.List;

public class MovieListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int ITEM = 0;
    private static final int LOADING = 1;
    private final List<Result> lists;
    private final Context context;
    public static final String BASE_URL_IMG = "https://image.tmdb.org/t/p/w185";

    private boolean isLoadingAdded = false;


    public MovieListAdapter(Context context, List<Result> lists) {
        this.context = context;
        this.lists = lists;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            default:
            case ITEM:
                viewHolder = getViewHolder(parent, inflater);
                break;
            case LOADING:
                View v2 = inflater.inflate(R.layout.item_progress, parent, false);
                viewHolder = new LoadingVH(v2);
                break;


        }
        return viewHolder;

    }

    @NonNull
    private RecyclerView.ViewHolder getViewHolder(ViewGroup parent, LayoutInflater inflater) {
        RecyclerView.ViewHolder viewHolder;
        View v1 = inflater.inflate(R.layout.recyclerlist, parent, false);
        viewHolder = new MyViewHolder(v1);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        Result list = lists.get(position);

        switch (getItemViewType(position)) {
            case ITEM:
                final MyViewHolder viewHolder = (MyViewHolder) holder;
                viewHolder.frameLayout.setAnimation( AnimationUtils.loadAnimation( context, R.anim.item_animation_fall_down ) );
                viewHolder.description.setText(list.getOverview());
                viewHolder.title.setText(list.getTitle());

                Glide.with(context).load(BASE_URL_IMG + list.getPosterPath())
                        .apply(new RequestOptions()
                        )
                        .into(viewHolder.posterImage);
                viewHolder.posterImage.setOnClickListener(view -> {

                    Intent intent = new Intent(context, MovieDetailActivity.class);
                    intent.putExtra("poster", lists.get(position).getPosterPath());
                    intent.putExtra("releaseDate", lists.get(position).getReleaseDate());
                    intent.putExtra("popularity", lists.get(position).getPopularity());
                    intent.putExtra("vote", lists.get(position).getVoteCount());
                    intent.putExtra("backDrop", lists.get(position).getBackdropPath());

                    intent.putExtra("overview", lists.get(position).getOverview());
                    intent.putExtra("title", lists.get(position).getTitle());
                    // Get the transition name from the string
                    String transitionName = context.getString(R.string.transition);

                    ActivityOptionsCompat options =

                            ActivityOptionsCompat.makeSceneTransitionAnimation((Activity) context,
                                    view,   // Starting view
                                    transitionName    // The String
                            );

                    ActivityCompat.startActivity(context, intent, options.toBundle());


                });
                break;
            case LOADING:
                break;
            default:
        }
    }


    @Override
    public int getItemCount() {
        return lists.size();
    }

    @Override
    public int getItemViewType(int position) {
        return (position == lists.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
    }
 /*
   Helpers

    */



    private void add(Result r) {
        lists.add(r);
        notifyItemInserted(lists.size() - 1);
    }

    public void addAll(List<Result> result) {
        for (Result list : result) {
            add(list);
        }
    }


    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new Result());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = lists.size() - 1;
        Result result = getItem(position);

        if (result != null) {
            lists.remove(position);
            notifyItemRemoved(position);
        }
    }

    private Result getItem(int position) {
        return lists.get(position);
    }

    private static class MyViewHolder extends RecyclerView.ViewHolder {
        final TextView description;
        final ImageView posterImage;
        final TextView title;
final FrameLayout frameLayout;

        private MyViewHolder(@NonNull View itemView) {
            super(itemView);

            description = itemView.findViewById(R.id.description);

            title = itemView.findViewById(R.id.title);
            posterImage = itemView.findViewById(R.id.poster_image);
            frameLayout=itemView.findViewById(R.id.frame_layout);
        }
    }


    private static class LoadingVH extends RecyclerView.ViewHolder {


        private LoadingVH(View itemView) {
            super(itemView);

        }
    }

}
