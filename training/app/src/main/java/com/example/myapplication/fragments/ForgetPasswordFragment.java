package com.example.myapplication.fragments;


import android.annotation.SuppressLint;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.greendaogenerator.User;
import com.example.greendaogenerator.UserDao;
import com.example.myapplication.R;
import com.example.myapplication.utilitis.GreenDao;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.Objects;


public class ForgetPasswordFragment extends Fragment implements View.OnClickListener{

    private static final String CHANNEL_ID ="my_notification" ;
    private static final int NOTIFICATION_ID = 1 ;
    private View view;
    private TextInputEditText email;
    private TextInputEditText password;
    private TextInputEditText confirmPassword;
    private TextInputEditText otp;
    private TextInputLayout passwordLayout;
    private TextInputLayout confirmPasswordLayout;
    private TextInputLayout emailLayout;
    private TextInputLayout otpLayout;
    private Button submit;
    private Button next1;
    private Button next2;
    private String strEmail;
    private String strOtp;
    private String strPassword;
private UserDao userDao;
private User user;
   private ProgressDialog progressDialog;
    private boolean validatePass=false;
    private boolean validateConfirmPass=false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_forget_password, container, false);
        init();
        userDao = ((GreenDao) Objects.requireNonNull(getActivity()).getApplication()).getDaoSession().getUserDao();

        submit.setOnClickListener(this);
        next1.setOnClickListener(this);
        next2.setOnClickListener(this);

        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Loading Please wait");
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        passwordLayout.setVisibility(View.GONE);
        otpLayout.setVisibility(View.GONE);
        confirmPasswordLayout.setVisibility(View.GONE);
        next2.setVisibility(View.GONE);
        submit.setVisibility(View.GONE);
        return view;
    }

    private void init() {
        email = view.findViewById(R.id.editTextEmail);
        password = view.findViewById(R.id.editTextPassword);
        passwordLayout = view.findViewById(R.id.textInputPassword);
        submit = view.findViewById(R.id.submit);
        next1 = view.findViewById(R.id.next1);
        next2 = view.findViewById(R.id.next2);
        emailLayout = view.findViewById(R.id.textInputEmail);
        otpLayout = view.findViewById(R.id.textInputOtp);
        otp = view.findViewById(R.id.editTextOtp);
        confirmPassword = view.findViewById(R.id.editTextPasswordConfirm);
        confirmPasswordLayout = view.findViewById(R.id.textInputPasswordConfirm);
        passwordValidate();

    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.submit) {

            strPassword = Objects.requireNonNull(password.getText()).toString().trim();
            validation();
        }
        if (v.getId() == R.id.next1) {

            strEmail = Objects.requireNonNull(email.getText()).toString().trim();
            emailValidate();


        }
        if (v.getId() == R.id.next2) {
            strOtp = Objects.requireNonNull(otp.getText()).toString().trim();


            otpValidate();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void getOtp() {
        createNotificationChannel();
        NotificationCompat.Builder builder=new NotificationCompat.Builder(Objects.requireNonNull(getContext()),CHANNEL_ID);
        builder.setSmallIcon(R.drawable.ic_person_black_24dp);
        builder.setContentTitle("OnCloud Notification");
        builder.setContentText("1234");
        builder.setPriority(NotificationCompat.PRIORITY_DEFAULT);

        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(getContext());
        notificationManagerCompat.notify(NOTIFICATION_ID,builder.build());
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @SuppressLint("ServiceCast")
    private void createNotificationChannel() {
        CharSequence name="Notify";
        int importance= NotificationManager.IMPORTANCE_DEFAULT;


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(CHANNEL_ID,name,importance);
            notificationChannel.setDescription("Otp");

       NotificationManager notificationManager= (NotificationManager) Objects.requireNonNull(getActivity()).getSystemService(Context.NOTIFICATION_SERVICE);
        Objects.requireNonNull(notificationManager).createNotificationChannel(notificationChannel);}
    }

    private void passwordValidate() {
        String passwordPattern = getString(R.string.password_pattern);

        password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
/*
for before text change  but here we use only after textChange
 */
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
/*
for on text change  but here we use only after textChange
 */
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().isEmpty()) {
                    passwordLayout.setError("password cannot be empty");
                    validatePass = false;

                } else if (!s.toString().matches(passwordPattern)) {
                    passwordLayout.setError("Password should contain one capital letter,one small letter, one number,one special character and size between 6to12");
                    validatePass = false;

                } else {
                    passwordLayout.setError(null);
                    passwordLayout.setErrorEnabled(false);

                    strPassword = s.toString().trim();
                    validatePass = true;
                }
            }
        });

        confirmPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
/*
for before text change  but here we use only after textChange
 */
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
/*
for on text change  but here we use only after textChange
 */
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().isEmpty()) {
                    confirmPasswordLayout.setError("confirm password cannot be empty");
                    validateConfirmPass = false;

                } else if (!s.toString().equals(strPassword)) {
                    confirmPasswordLayout.setError("confirm password and password should be same");
                    validateConfirmPass = false;

                } else {
                    confirmPasswordLayout.setError(null);
                    confirmPasswordLayout.setErrorEnabled(false);

                    validateConfirmPass = true;
                }
            }
        });


    }

    private void validation() {
        if(!validatePass){
            passwordLayout.setError("");
        }else if(!validateConfirmPass){
            confirmPasswordLayout.setError("");
        }else  {
            progressDialog.dismiss();
            user.setPassword(Objects.requireNonNull(password.getText()).toString().trim());
            userDao.update(user);
            FragmentManager manager = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.replace(R.id.container, new LoginFragment()).commit();
        }

    }

    private void otpValidate() {

        if (strOtp.isEmpty()) {
            otp.setError("Field cannot be empty");
        } else {
            if (strOtp.equals("1234")) {
                otpLayout.setVisibility(View.GONE);
                next2.setVisibility(View.GONE);
                submit.setVisibility(View.VISIBLE);
                passwordLayout.setVisibility(View.VISIBLE);
                confirmPasswordLayout.setVisibility(View.VISIBLE);

            } else {
                otpLayout.setError("Enter correct OTP");
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void emailValidate() {
        user = userDao
                .queryBuilder().where(UserDao.Properties.Email.eq(strEmail)).build().unique();
        if(user!=null){
        String storedName = user.getEmail();
        if (strEmail.isEmpty()) {
            email.setError("Field cannot be empty");
        } else {
            if (storedName.equals(strEmail)) {
                emailLayout.setVisibility(View.GONE);
                otpLayout.setVisibility(View.VISIBLE);
                next1.setVisibility(View.GONE);
                next2.setVisibility(View.VISIBLE);
                getOtp();

            } else {
                emailLayout.setError("Enter Valid EmailId");
            }
        }}
        else {
            emailLayout.setError("Enter Valid EmailId");
        }
    }



}
