package com.example.myapplication.activities;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.greendaogenerator.User;
import com.example.greendaogenerator.UserDao;
import com.example.myapplication.R;
import com.example.myapplication.fragments.ProfileBottomSheet;
import com.example.myapplication.utilitis.GreenDao;
import com.example.myapplication.utilitis.SharedPref;
import com.google.android.material.textfield.TextInputEditText;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.Objects;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class MyProfileActivity extends AppCompatActivity {

    private TextInputEditText name;
    private TextInputEditText email;
    private TextInputEditText age;
    private ImageView profileImage;
    private UserDao userDao;
    private User user;
    private TextView changePassword;
    private static String change="" ;
    private static final int PERMISSION_REQUEST_CODE = 100;
    public static String getChange() {
        return change;
    }
    public static void setChange(String change) {
        MyProfileActivity.change = change;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);
        long key = SharedPref.shareKey(getApplicationContext());
        userDao = ((GreenDao) getApplication()).getDaoSession().getUserDao();
        user = userDao
                .queryBuilder().where(UserDao.Properties.Id.eq(key)).build().unique();
        init();
        Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00a5ff")));

        changePassword.setOnClickListener(v -> {
            Intent intent =new Intent(MyProfileActivity.this,ChangePasswordActivity.class);
            startActivity(intent);
        });

        name.setOnTouchListener((v, event) -> {
            final int DRAWABLE_RIGHT = 2;

                if (event.getAction() == MotionEvent.ACTION_UP&&event.getRawX() >= (name.getRight() - name.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {

                  setChange("name");
                    new ProfileBottomSheet().show(getSupportFragmentManager(),"Dialog");
                }

            return false;
        });
        age.setOnTouchListener((v, event) -> {
            final int DRAWABLE_RIGHT = 2;

                if (event.getAction() == MotionEvent.ACTION_UP&&event.getRawX() >= (name.getRight() - name.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {

                    showAlert();
                }

            return false;
        });
        email.setOnTouchListener((View v, MotionEvent event) -> {
            final int DRAWABLE_RIGHT = 2;

                if (event.getAction() == MotionEvent.ACTION_UP&&event.getRawX() >= (name.getRight() - name.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {

                   setChange("email");
                    new ProfileBottomSheet().show(getSupportFragmentManager(),"Dialog");
                }

            return false;
        });


        profileImage.setOnClickListener(v -> {
            if (ContextCompat.checkSelfPermission(this, READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(this, WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(this, new String[]{READ_EXTERNAL_STORAGE, WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);

            }else {
                gallery();
            }
        });
        fetchData();
    }

    private void gallery() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(galleryIntent, 101);
    }

    private void fetchData() {

        if (user != null) {
            name.setText(user.getUserName());
            age.setText(user.getAge());
            email.setText(user.getEmail());
            if (user.getProfileImage() != null) {
                Glide.with(this).load(new File(user.getProfileImage()))
                        .apply(RequestOptions.circleCropTransform()).apply(RequestOptions.placeholderOf(R.drawable.ic_person_black_24dp))
                        .into(profileImage);
            } else {
                Glide.with(this).load("")
                        .apply(RequestOptions.circleCropTransform()).apply(RequestOptions.placeholderOf(R.drawable.ic_person_black_24dp))
                        .into(profileImage);
            }
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void showAlert() {

// Get Current Date
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(MyProfileActivity.this,
                (group, year, monthOfYear, dayOfMonth) -> {

                    if (year < mYear) {
                        int age1 = mYear - year;
                        age.setText(new StringBuilder().append(age1).append(" years"));
                        user.setAge(Objects.requireNonNull(age.getText()).toString().trim());
                        userDao.update(user);
                    }

                }, mYear, mMonth, mDay);

        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.show();
    }

    private void init() {

        name = findViewById(R.id.name_editText);
        email = findViewById(R.id.email_editText);
        age = findViewById(R.id.age_editText);
        profileImage = findViewById(R.id.profile_image);
        changePassword=findViewById(R.id.change_password);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 101 && data != null) {
            Uri contentURI = data.getData();
            try {

                String imagePath = getRealPathFromURI(contentURI);
                user.setProfileImage(imagePath);

                userDao.update(user);
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), contentURI);

                profileImage.setImageBitmap(bitmap);

            } catch (IOException e) {
                Log.e("exception", Objects.requireNonNull(e.getMessage()));
                Toast.makeText(getApplicationContext(), "Failed!", Toast.LENGTH_SHORT).show();
            }
        }
        }

    private String getRealPathFromURI(Uri uri) {
        Cursor cursor = null;
        try {
         cursor = getApplicationContext().getContentResolver().query(uri, null, null, null, null);
        assert cursor != null;
        Objects.requireNonNull(cursor).moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }finally {

                Objects.requireNonNull(cursor).close();

        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                gallery();
                Toast.makeText(this, "Permission granted", Toast.LENGTH_SHORT).show();
            } else {

                Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show();

            }

        }}
}
