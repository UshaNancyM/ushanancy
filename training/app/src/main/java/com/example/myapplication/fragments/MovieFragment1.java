package com.example.myapplication.fragments;


import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.adapter.MovieListAdapter;
import com.example.myapplication.apiservices.ApiClient;
import com.example.myapplication.apiservices.ApiInterface;
import com.example.myapplication.model.Result;
import com.example.myapplication.model.TopRatedMovies;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class MovieFragment1 extends Fragment {


    private final List<Result> results = new ArrayList<>();
    private RecyclerView recyclerView;
    private MovieListAdapter movieListAdapter;


    private boolean isLoading = false;
    private boolean isLastPage = false;
    private static final int TOTAL_PAGES = 2;
    private GridLayoutManager layoutManager;
    private int currentPage =1;



    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
          View  view = inflater.inflate(R.layout.fragment_blank_fragment4, container, false);
        // If we have a saved state then we can restore it now


        recyclerView = view.findViewById(R.id.recycler2);

        if (Objects.requireNonNull(getActivity()).getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            if (isTablet(Objects.requireNonNull(getContext()))) {
                layoutManager = new GridLayoutManager(getContext(), 2);
            } else {
                layoutManager = new GridLayoutManager(getContext(), 1);
            }

        } else {
            if (isTablet(Objects.requireNonNull(getContext()))) {
                layoutManager = new GridLayoutManager(getContext(), 3);
            } else {
                layoutManager = new GridLayoutManager(getContext(), 2);
            }

        }
        movieListAdapter = new MovieListAdapter(getContext(), results);

        getList();


        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                int visibleItemCount = layoutManager.getChildCount();
                int totalItemCount = layoutManager.getItemCount();
                int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();


                if (!isLoading && !isLastPage && (visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                        && firstVisibleItemPosition >= 0
                        && totalItemCount >= TOTAL_PAGES) {
                    isLoading = true;
                    currentPage += 1;


                    loadNextPage();
                }
            }


        });

        return view;
    }


    private void loadNextPage() {
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<TopRatedMovies> call = apiService.getMovies(getString(R.string.my_api_key), "en_US", currentPage);

        call.enqueue(new Callback<TopRatedMovies>() {
            @Override
            public void onResponse(@NonNull Call<TopRatedMovies> call, @NonNull Response<TopRatedMovies> response) {
                if (response.isSuccessful()) {
                    movieListAdapter.removeLoadingFooter();
                    isLoading = false;
                    TopRatedMovies topRatedMovies = response.body();


                    assert topRatedMovies != null;
                    movieListAdapter.addAll(topRatedMovies.getResults());
                    movieListAdapter.notifyDataSetChanged();

                    recyclerView.setLayoutManager(layoutManager);
                    recyclerView.setAdapter(movieListAdapter);
                    if (currentPage < TOTAL_PAGES) movieListAdapter.addLoadingFooter();
                    else isLastPage = true;
                }
            }

            @Override
            public void onFailure(@NonNull Call<TopRatedMovies> call, @NonNull Throwable t) {
                Log.d("TAG", "Response = " + t.toString());
            }
        });
    }

    private void getList() {
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<TopRatedMovies> call = apiService.getMovies(getString(R.string.my_api_key), "en_US", currentPage);

        call.enqueue(new Callback<TopRatedMovies>() {
            @Override
            public void onResponse(@NonNull Call<TopRatedMovies> call, @NonNull Response<TopRatedMovies> response) {
                if (response.isSuccessful()) {
                    TopRatedMovies topRatedMovies = response.body();


                    if (topRatedMovies != null) {
                        movieListAdapter.addAll(topRatedMovies.getResults());
                    }
                    movieListAdapter.notifyDataSetChanged();
                    recyclerView.setLayoutManager(layoutManager);
                    recyclerView.setAdapter(movieListAdapter);

                    if (currentPage <= TOTAL_PAGES) movieListAdapter.addLoadingFooter();
                    else isLastPage = true;
                }
            }

            @Override
            public void onFailure(@NonNull Call<TopRatedMovies> call, @NonNull Throwable t) {
                Log.d("TAG", "Response = " + t.toString());
            }
        });
    }

    private boolean isTablet(Context context) {
        return (context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_LARGE;
    }

}